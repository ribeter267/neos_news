<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TYPO3.' . $_EXTKEY,
	'News',
	array(
		'News' => 'list, edit, new, show, create, update, delete',
		'Category' => 'list',
	),
	// non-cacheable actions
	array(
		'News' => 'delete',
	)
);

?>