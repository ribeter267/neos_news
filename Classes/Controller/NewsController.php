<?php
namespace TYPO3\NeosNews\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, neos.org.cn
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package neos_news
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class NewsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * newsRepository
	 *
	 * @var \TYPO3\NeosNews\Domain\Repository\NewsRepository
	 * @inject
	 */
	protected $newsRepository;
	
	
	/**
	 * newsRepository
	 *
	 * @var \TYPO3\NeosNews\Domain\Repository\CategoryRepository
	 * @inject
	 */
	protected $categoryRepository;
	
	
	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$newss = $this->newsRepository->findAll();
		$this->view->assign('newss', $newss);
	}

	/**
	 * action show
	 *
	 * @param \TYPO3\NeosNews\Domain\Model\News $news
	 * @return void
	 */
	public function showAction(\TYPO3\NeosNews\Domain\Model\News $news) {
		$this->view->assign('news', $news);
	}
	
	
	/**
	 * action edit
	 *
	 * @param \TYPO3\NeosNews\Domain\Model\News $newNews
	 * @dontvalidate $newNews
	 * @return void
	 */
	public function editAction(\TYPO3\NeosNews\Domain\Model\News $news = null) {
		
		$news_uid = (int)$this->request->getArgument('news');
		$news = $this->newsRepository->findByUid($news_uid);
		
		$this->view->assignMultiple(array(
			'news' => $news,
			'categories' => $this->categoryRepository->findAll(),
		));
		
	}
	
	
	/**
	 * action update
	 *
	 * @param \TYPO3\NeosNews\Domain\Model\News $news
	 * @return void
	 */
	public function updateAction(\TYPO3\NeosNews\Domain\Model\News $news) {
		$this->newsRepository->update($news);
		$this->flashMessageContainer->add('Your News was updated.');
		$this->redirect('list');
	}
	

	/**
	 * action new
	 *
	 * @param \TYPO3\NeosNews\Domain\Model\News $newNews
	 * @dontvalidate $newNews
	 * @return void
	 */
	public function newAction(\TYPO3\NeosNews\Domain\Model\News $newNews = NULL) {
		$this->view->assignMultiple(array(
			'newNews' => $newNews,
			'categories' => $this->categoryRepository->findAll(),
		));
	}

	/**
	 * action create
	 *
	 * @param \TYPO3\NeosNews\Domain\Model\News $newNews
	 * @return void
	 */
	public function createAction(\TYPO3\NeosNews\Domain\Model\News $newNews) {
		$this->newsRepository->add($newNews);
		$this->flashMessageContainer->add('Your new News was created.');
		$this->redirect('list');
	}


	/**
	 * action delete
	 *
	 * @param \TYPO3\NeosNews\Domain\Model\News $news
	 * @return void
	 */
	public function deleteAction(\TYPO3\NeosNews\Domain\Model\News $news) {
		$this->newsRepository->remove($news);
		$this->flashMessageContainer->add('Your News was removed.');
		$this->redirect('list');
	}

}
?>