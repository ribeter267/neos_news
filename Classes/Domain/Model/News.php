<?php
namespace TYPO3\NeosNews\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, neos.org.cn
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package neos_news
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class News extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * 新闻标题
	 *
	 * @var \string
	 */
	protected $title;

	/**
	 * 新闻正文
	 *
	 * @var \string
	 */
	protected $content;

	/**
	 * 新闻分类
	 *
	 * @var \TYPO3\NeosNews\Domain\Model\Category
	 */
	protected $categoryid;

	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the content
	 *
	 * @return \string $content
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Sets the content
	 *
	 * @param \string $content
	 * @return void
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * Returns the categoryid
	 *
	 * @return \TYPO3\NeosNews\Domain\Model\Category $categoryid
	 */
	public function getCategoryid() {
		return $this->categoryid;
	}

	/**
	 * Sets the categoryid
	 *
	 * @param \TYPO3\NeosNews\Domain\Model\Category $categoryid
	 * @return void
	 */
	public function setCategoryid(\TYPO3\NeosNews\Domain\Model\Category $categoryid) {
		$this->categoryid = $categoryid;
	}

}
?>